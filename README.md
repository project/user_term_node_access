# User term node access

## Description

Allows managing node access by user terms / tags. Users can be grouped together
through taxonomy terms. Access per node can be configured, so the node can be
viewed by the selected user group(s).

## Installation

* Install the module as usual.
* A new vocabulary "User node access" will be created on install.
* Go to /admin/structure/taxonomy and add terms to this vocabulary (e.g.
  'Customers', 'Suppliers', 'Employees', ...)
* Go to the content type overview page (/admin/structure/types), and for all
  node types you want to enable this module for, perform these actions:
  * Edit the form display (e.g. /admin/structure/types/manage/page/form-display)
  * Enable the permission fields you wish to control per node:
    * User term(s) with view access (preferably use `options_buttons` widget)
    * User term(s) with update access (preferably use `options_buttons` widget)
    * User term(s) with delete access (preferably use `options_buttons` widget)
* Now do the same for the user entity: (preferably use `options_buttons` widget)
  * Edit the form display settings for user entities (/admin/config/people/accounts/form-display)
  * Enable the permission fields you wish to control per node:
    * User term(s) with view access (preferably use `options_buttons` widget)
    * User term(s) with update access (preferably use `options_buttons` widget)
    * User term(s) with delete access (preferably use `options_buttons` widget)
* Go to the `Status report` page (/admin/reports/status) and rebuild node
  permissions
* Now you can test the configured logic:
  * Create a new user, and set the field `User term(s) with view access` to e.g.
    'Customers'
  * Now create a new node, and set the field `User term(s) with view access` to
  e.g. 'Customers'
  * Only the 'Customer' user should be able to view the node. Users with another
    user group will not be able to view the node.

## Configuration

* You can change the vocabulary that this module users for node access. E.g. if
  your Drupal installation already has a vocabulary that should be used for this
  you can go to the settings page (/admin/config/people/user_term_node_access)
  and change the default vocabulary to your own. The vocabulary created by this
  module can optionally be deleted, if there is no use for it.
