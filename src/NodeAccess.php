<?php

namespace Drupal\user_term_node_access;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\TermInterface;
use Drupal\user\UserInterface;
use Drupal\user\UserStorageInterface;

/**
 * Service to manage node access for user_term_node_access.
 */
class NodeAccess {

  /**
   * The user entity storage.
   *
   * @var \Drupal\user\UserStorageInterface
   */
  protected UserStorageInterface $userStorage;

  /**
   * Constructs a NodeAccess object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->userStorage = $entity_type_manager->getStorage('user');
  }

  /**
   * Get node grants for a user.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current Drupal user.
   * @param string $op
   *   The node operation to be performed: 'view', 'update', or 'delete'.
   *
   * @return array
   *   List of grants.
   */
  public function getNodeGrantsForUser(AccountInterface $account, string $op): array {
    if ($account->isAnonymous()) {
      return [];
    }
    $user = $this->userStorage->load($account->id());
    if (!$user instanceof UserInterface) {
      return [];
    }

    $grants = [];
    $field = 'user_term_node_access_' . $op;
    if ($user->hasField($field) && !$user->get($field)->isEmpty()) {
      $user_terms = $user->get($field)->referencedEntities();
      $user_term_ids = array_map(function (Term $term) {
        return $term->id();
      }, $user_terms);
      $grants['user_term_node_access'] = $user_term_ids;
    }
    return $grants;
  }

  /**
   * Get node access records.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node that has just been saved.
   *
   * @return array
   *   An array of grants
   */
  public function getNodeAccessRecords(NodeInterface $node): array {
    $grants = [];
    $user_term_permissions = [];
    // Group node access grants per user term.
    foreach (['view', 'update', 'delete'] as $op) {
      if ($node->hasField('user_term_node_access_' . $op) && !$node->get('user_term_node_access_' . $op)->isEmpty()) {
        foreach ($node->get('user_term_node_access_' . $op)->referencedEntities() as $user_term) {
          if ($user_term instanceof TermInterface) {
            $user_term_permissions[$user_term->id()][] = $op;
          }
        }
      }
    }

    if (!empty($user_term_permissions)) {
      foreach ($user_term_permissions as $user_term_id => $ops) {
        $grants[] = [
          'realm' => 'user_term_node_access',
          'gid' => $user_term_id,
          'grant_view' => $node->isPublished() && in_array('view', $ops) ? 1 : 0,
          'grant_update' => in_array('update', $ops) ? 1 : 0,
          'grant_delete' => in_array('delete', $ops) ? 1 : 0,
        ];
      }
    }

    return $grants;
  }

}
